jQuery(document).ready(function($) {

	$('#maximage').maximage({
		cycleOptions: {
			fx: 'fade',
			speed: 400,
			timeout: 3000
		},
		onFirstImageLoaded: function() {
			jQuery('#cycle-loader').hide();
			jQuery('#maximage').fadeIn('fast');
		}

	});
	$('.in-slide-content').delay(1200).fadeIn();

	$(document).bind('keypress', function(e) {
		if (e.keyCode == 37)
			$('#maximage').cycle('prev');
		else if (e.keyCode == 39)
			$('#maximage').cycle('next');
	});

	$('#ah_prev').click(function() {
		$('#maximage').cycle('prev');
	});
	$('#ah_next').click(function() {
		$('#maximage').cycle('next');
	});

	var prevNext = $('#ah_prev, #ah_next');


	/**********************
	AH Menu scripts
	******************/

	var menuToggle = function(toggler, container) {
		var menuToggler = toggler;
		var menuContainer = container;
		menuToggler.on('click', function() {
			menuContainer.toggleClass('closed');
			menuToggler.toggleClass('closed');
		})
	}
	$('#content').on('click', function(){
		if(!$('#header').hasClass('closed')){
			$('#header').addClass('closed');
			$('.menu-icon').addClass('closed');
		}
	})

var closeMenu = setTimeout(function() {
	$('#header').addClass('closed');
	$('.menu-icon').addClass('closed');
}, 5000);

	menuToggle($('.menu-icon'), $('#header'));
	$('#header').hover(function() {
		clearTimeout(closeMenu);
	});
})
