<?php
/**
 * The template for displaying gallery pages.
 Template Name: ah Gallery
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
<div id="content"><?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<?php the_content(); ?>
	<?php endwhile; ?>
	<?php if(!is_front_page()){ ?>
		<div id='ah_prev'>
		Prev
	</div>
	<div id='ah_next'>
		Next
	</div> <?php
	} ?>
</div>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>