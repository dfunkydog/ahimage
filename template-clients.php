<?php
/*
Template Name: Our Clients Page
*/
?>
<?php get_header();?>
<div class="editorial">
<div id="clients">
<h2>Our Clients</h2>
<div class="twoCol left">
	<ul class="clientList">
		<?php $args = array( 'post_type' => 'our_clients', 'showposts' => 20 );
          $loop = new WP_Query( $args );
               while ( $loop->have_posts() ) : $loop->the_post();?>
               <li class="hasImage hitarea">
               <?php if(!get_field('client_site_url')) { ?>
	               <h2><?php the_title()?></h2>
	               <?php } else { ?>
	               <h2><a href="<?php the_field('client_site_url');?>"><?php the_title()?></a></h2>
	               <?php };?>
				   	<?php $image = get_field('client_logo');?>
				   	<img src="<?php echo $image['url'];?>" />
				   	<div class="clientDescription">
					   	<?php the_content();?>
				   	</div>
               </li>

               <?php endwhile;?>
	</ul>
</div>
</div>
</div>


<?php get_footer();?>
