#AHIMAGE WORDPRESS THEME#

###Created by 416 Studios.###

- - - - - - - - - - - - - - - - - - - - - - -

AHIMAGE is a wordpress site developed for ahimagestudios.com


##NOTA BENE##
------------------
I have elected not to include css files in the
repo.
The code in .scss files are "prefix-free",
Vendor prefixs are added after css compilation
via "autoprefixer". How that is done is very
much dependant on the dev environment.

Take a look and edit config.rb if necessary.

