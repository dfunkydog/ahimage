module.exports = function(grunt) {

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({



    // add prefixes only on dist
    autoprefixer: {
      options: {
        browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1']
      },
      dist: {
        files: [{
          src: 'css/screen.css',
          dest: 'css/screen.css'
        }]
      }
    },



  });



  grunt.registerTask('default', [
    'autoprefixer:dist'
    ]);

};